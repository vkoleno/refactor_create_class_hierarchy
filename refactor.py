import pygame
import random
import math

SCREEN_DIM = (800, 600)


class Vec2d:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, vector):
        return Vec2d(self.x + vector.x, self.y + vector.y)
        
    def __sub__(self, vector):
        return Vec2d(self.x - vector.x, self.y - vector.y)
        
    def __mul__(self, k):
        return Vec2d(self.x * k, self.y * k)
        
    def __len__(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5
        
    def scal_mul(self, vector):
        return self.x * vector.x + self.y * vector.y
        
    def int_pair(self):
        return self.x, self.y

    def vec(self, vector):
        return Vec2d(vector.x - self.x, vector.y - self.y)
        
        
class Polyline:

    def __init__(self, points=None, speeds=None, steps=35):
        self.points = points or []
        self.speeds = speeds or []
        self.steps = steps
        
    def add_point(self, vector):
        self.points.append(Vec2d(vector[0], vector[1]))
        self.speeds.append((random.random() * 2, random.random() * 2))
        
    def set_points(self):
        points = self.points
        speeds = self.speeds
        for p in range(len(self.points)):
            points[p] = points[p] + Vec2d(speeds[p][0], speeds[p][1])
            if points[p].x > SCREEN_DIM[0] or points[p].x < 0:
                speeds[p] = (- speeds[p][0], speeds[p][1])
            if points[p].y > SCREEN_DIM[1] or points[p].y < 0:
                speeds[p] = (speeds[p][0], -speeds[p][1])

    def draw_points(self, width=3, color=(255, 255, 255)):
        for p in self.points:
            pygame.draw.circle(gameDisplay, color,
                               (int(p.x), int(p.y)), width)

    def reset(self):
        self.points = []
        self.speeds = []


class Knot(Polyline):

    def get_knot(self):
        points = self.points
        if len(points) < 3:
            return []
        res = []
        for i in range(-2, len(points) - 2):
            ptn = []
            ptn.append((points[i] + points[i + 1]) * 0.5)
            ptn.append(points[i + 1])
            ptn.append((points[i + 1] + points[i + 2]) * 0.5)

            res.extend(self.get_points(ptn))
        return res

    def get_point(self, points, alpha, deg=None):
        points = points
        if deg is None:
            deg = len(points) - 1
        if deg == 0:
            return points[0]
        return (points[deg] * alpha) + self.get_point(points, alpha, deg - 1) * (1 - alpha)

    def get_points(self, base_points):
        count = self.steps
        alpha = 1 / count
        res = []
        for i in range(count):
            res.append(self.get_point(base_points, i * alpha))
        return res

    def draw_points(self, width=3, line_color=(255, 255, 255), point_color=(255, 255, 255)):
        points = self.get_knot()
        for p in self.points:
            pygame.draw.circle(gameDisplay, point_color,
                               (int(p.x), int(p.y)), width)
        for p_n in range(-1, len(points) - 1):
            pygame.draw.line(gameDisplay, line_color, (int(points[p_n].x), int(points[p_n].y)),
                             (int(points[p_n + 1].x), int(points[p_n + 1].y)), width)

    def speed_up(self, k=1.5):
        self.speeds = [tuple([point_speed[0] * k, point_speed[1] * k]) for point_speed in self.speeds]

    def speed_down(self, k=0.5):
        self.speeds = [tuple([point_speed[0] * k, point_speed[1] * k]) for point_speed in self.speeds]

    def delete_point(self):
        if len(self.points) > 0:
            del self.points[-1]
            del self.speeds[-1]


# ��������� �������
def draw_help(knot):
    gameDisplay.fill((50, 50, 50))
    font1 = pygame.font.SysFont("courier", 24)
    font2 = pygame.font.SysFont("serif", 24)
    data = []
    data.append(["F1", "Show Help"])
    data.append(["R", "Restart"])
    data.append(["P", "Pause/Play"])
    data.append(["1-5", "Add/Select new line"])
    data.append(["Up", "Increase points speed"])
    data.append(["Down", "Decrease points speed"])
    data.append(["Delete", "Delete last added point"])
    data.append(["Num+", "More points"])
    data.append(["Num-", "Less points"])
    data.append(["", ""])
    data.append([str(knot.steps), "Current points"])

    pygame.draw.lines(gameDisplay, (255, 50, 50, 255), True, [
                      (0, 0), (800, 0), (800, 600), (0, 600)], 5)
    for i, text in enumerate(data):
        gameDisplay.blit(font1.render(
            text[0], True, (128, 128, 255)), (100, 100 + 30 * i))
        gameDisplay.blit(font2.render(
            text[1], True, (128, 128, 255)), (200, 100 + 30 * i))


# �������� ���������
if __name__ == "__main__":

    pygame.init()
    gameDisplay = pygame.display.set_mode(SCREEN_DIM)
    pygame.display.set_caption("MyScreenSaver")

    working = True
    show_help = False
    pause = True

    knots = [Knot()]
    knots_indeces = {pygame.K_1: 0}
    current_knot = None

    hue = 0
    color = pygame.Color(0)

    while working:

        if current_knot is None:
            current_knot = knots[knots_indeces[pygame.K_1]]
        else:
            pass
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                working = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    working = False
                if event.key == pygame.K_r:
                    current_knot.reset()
                if event.key == pygame.K_p:
                    pause = not pause
                if event.key == pygame.K_KP_PLUS:
                    current_knot.steps += 1
                if event.key == pygame.K_F1:
                    show_help = not show_help
                if event.key == pygame.K_KP_MINUS:
                    current_knot.steps -= 1 if current_knot.steps > 1 else 0
                if event.key == pygame.K_UP:
                    current_knot.speed_up()
                if event.key == pygame.K_DOWN:
                    current_knot.speed_down()
                if event.key == pygame.K_DELETE:
                    current_knot.delete_point()
                if event.key in [pygame.K_1, pygame.K_2, pygame.K_3, pygame.K_4, pygame.K_5]:
                    if event.key not in knots_indeces:
                        new_knot = Knot()
                        knots.append(new_knot)
                        knots_indeces[event.key] = knots.index(new_knot)
                    current_knot = knots[knots_indeces[event.key]]
                    print(knots)
                    print(knots_indeces)

            if event.type == pygame.MOUSEBUTTONDOWN:
                current_knot.add_point(event.pos)

        gameDisplay.fill((0, 0, 0))
        hue = (hue + 1) % 360
        color.hsla = (hue, 100, 50, 100)
        for knot in knots:
            knot.draw_points(line_color=color)
        if not pause:
            for knot in knots:
                knot.set_points()
        if show_help:
            draw_help(current_knot)

        pygame.display.flip()

    pygame.display.quit()
    pygame.quit()
    exit(0)
